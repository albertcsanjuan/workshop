<?php

namespace App\Http\Controllers;

use App\LicenseRegistration;
use Illuminate\Http\Request;

class LicenseRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LicenseRegistration  $licenseRegistration
     * @return \Illuminate\Http\Response
     */
    public function show(LicenseRegistration $licenseRegistration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LicenseRegistration  $licenseRegistration
     * @return \Illuminate\Http\Response
     */
    public function edit(LicenseRegistration $licenseRegistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LicenseRegistration  $licenseRegistration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LicenseRegistration $licenseRegistration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LicenseRegistration  $licenseRegistration
     * @return \Illuminate\Http\Response
     */
    public function destroy(LicenseRegistration $licenseRegistration)
    {
        //
    }
}
