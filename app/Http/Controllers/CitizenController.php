<?php

namespace App\Http\Controllers;

use App\Citizen;
use Illuminate\Http\Request;

class CitizenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$citizens = \App\Citizen::latest('id')->paginate(10);
        return view('citizen/index', [ 'citizens' => $citizens, 'active' => 'citizens' ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('citizen/create',['active' => 'citizens']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $validatedData = $request->validate([
        	"first_name" 	=> 'required|string',
			"middle_name" 	=> 'required|string',
			"last_name" 	=> 'required|string',
			"gender" 		=> 'required|string|in:M,F',
			"birthdate" 	=> 'required|date|before:today',
			"address" 		=> 'required|string',
			"zip" 			=> 'required',
        ]);

        // store that data into our table
        $citizen = \App\Citizen::create($validatedData);

        // diplay html showing our submitted data
        return view('citizen/show', ['citizen' => $citizen, 'active' => 'citizens']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function show(Citizen $citizen)
    {	
        return view('citizen/show', ['citizen' => $citizen, 'active' => 'citizens']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function edit(Citizen $citizen)
    {
        return view('citizen/edit', ['citizen' => $citizen, 'active' => 'citizens']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Citizen $citizen)
    {
		// validate the data
        $validatedData = $request->validate([
        	"first_name" 	=> 'required|string',
			"middle_name" 	=> 'required|string',
			"last_name" 	=> 'required|string',
			"gender" 		=> 'required|string|in:M,F',
			"birthdate" 	=> 'required|date|before:today',
			"address" 		=> 'required|string',
			"zip" 			=> 'required',
        ]);

        // update that data into our table
        $citizen->update($validatedData);

        // diplay html showing our submitted data
        return redirect()->route('citizen.show',$citizen); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Citizen  $citizen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citizen $citizen)
    {
        // Delete the row from the database
        $citizen->delete();

        // Return to the index page
     	return redirect()->route('citizen.index');
    }
}
