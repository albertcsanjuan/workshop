<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;

class TestController extends Controller
{
    public function testSubmit(Request $request) {

    	$r = $request->only(['first_name','middle_name','last_name','gender','birthdate','address','zip','submitted_by']);
  		Log::info($r);
    	$data['r'] = $request;
    	return view('test.citizen',$data);

    }
}
