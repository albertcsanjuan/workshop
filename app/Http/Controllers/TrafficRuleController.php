<?php

namespace App\Http\Controllers;

use App\TrafficRule;
use Illuminate\Http\Request;

class TrafficRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrafficRule  $trafficRule
     * @return \Illuminate\Http\Response
     */
    public function show(TrafficRule $trafficRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrafficRule  $trafficRule
     * @return \Illuminate\Http\Response
     */
    public function edit(TrafficRule $trafficRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrafficRule  $trafficRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrafficRule $trafficRule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrafficRule  $trafficRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrafficRule $trafficRule)
    {
        //
    }
}
