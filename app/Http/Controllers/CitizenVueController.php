<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CitizenVueController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
        	return \App\Citizen::latest('id')->limit(10)->get();
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('citizen/vue/create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            // validate the data
            $validatedData = $request->validate([
            	"first_name" 	=> 'required|string',
    			"middle_name" 	=> 'required|string',
    			"last_name" 	=> 'required|string',
    			"gender" 		=> 'required|string|in:M,F',
    			"birthdate" 	=> 'required|date|before:today',
    			"address" 		=> 'required|string',
    			"zip" 			=> 'required',
            ]);

            // store that data into our table
            $citizen = \App\Citizen::create($validatedData);

            // diplay html showing our submitted data
            // return view('citizen/show', ['citizen' => $citizen]);
        }
}
