<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    public function citizen() {
    	return $this->belongsTo('\App\Citizen');
    }

    public function registrations() {
    	return $this->hasMany('App\LicenseRegistration');
    }

    public function violations() {
    	return $this->hasMany('App\Violation');
    }
}
