<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	public $incrementing = false;
	public $primaryKey = 'state_code';
	public $keyType = 'string';

    public function state() {
    	return $this->belongsTo('App\State','state_code','state_code');
    }
}
