<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	public $incrementing = false;
	public $primaryKey = 'state_code';
	
    public function cities() {
    	return $this->hasMany('App\City','state_code','state_code');
    }
}
