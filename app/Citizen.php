<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
	protected $guarded = [];
	protected $dates = ['birthdate'];

    public function license() {
    	return $this->hasOne('App\License');
    }

    public function getAgeAttribute() {
    	return $this->birthdate->diffInYears();
    }
}
