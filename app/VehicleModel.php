<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
	public $timestamps = false;
	
    public function vehicles() {
    	return $this->hasMany('App\Vehicle','model_id');
    }
}
