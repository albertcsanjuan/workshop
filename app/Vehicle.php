<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function model() {
    	return $this->belongsTo('App\VehicleModel','model_id');
    }
    public function registrations() {
    	return $this->hasMany('App\VehicleRegistration');
    }
    public function violations() {
    	return $this->hasMany('App\Violation');
    }
}
