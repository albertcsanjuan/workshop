<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Violation extends Model
{
    public function license() {
    	return $this->belongsTo('\App\License');
    }

    public function rule() {
    	return $this->belongsTo('\App\TrafficRule');
    }

    public function vehicle() {
    	return $this->belongsTo('\App\Violation');
    }
}
