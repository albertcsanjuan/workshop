<?php

use Faker\Generator as Faker;

$factory->define(App\Vehicle::class, function (Faker $faker) {
	$model = App\VehicleModel::inRandomOrder()->first();

    return [
        'model_id'	=> $model->id,
        'year'		=> $model->year,
        'color'		=> $faker->colorName,
        'plate_number'	=> strtoupper($faker->unique()->bothify('##-????-####')),
        'displacement'	=> $faker->randomElement(['1.0','1.2','1.4','1.5','1.6','1.8','2.0','2.2','2.4','2.5','2.8','3.0','3.5','3.6','3.8','4.0','4.5','4.6','4.8','5.0','5.6','6.0'])
    ];
});
