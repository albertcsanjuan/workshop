<?php

use Faker\Generator as Faker;

$factory->define(App\LicenseRegistration::class, function (Faker $faker) {
	$zip = App\City::inRandomOrder()->first()->zip;

    return [
        'zip_code' => $zip
    ];
});
