<?php

use Faker\Generator as Faker;

$factory->define(App\Violation::class, function (Faker $faker) {
    $r = App\TrafficRule::inRandomOrder()->first()->id;
    $v = App\Vehicle::inRandomOrder()->first()->id;

    return [
        'vehicle_id' => $v,
        'traffic_rule_id' => $r
    ];
});
