<?php

use Faker\Generator as Faker;

$factory->define(App\License::class, function (Faker $faker) {
    return [
        // 'citizen_id' => $faker->numberBetween(1,100000)->unique(),
        'license_no' => strtoupper($faker->unique()->bothify('LIC-????-####')),
    ];
});
