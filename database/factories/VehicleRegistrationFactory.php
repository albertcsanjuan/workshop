<?php

use Faker\Generator as Faker;

$factory->define(App\VehicleRegistration::class, function (Faker $faker) {
	$zip = App\City::inRandomOrder()->first()->zip;
	// $citizen = App\Citizen::inRandomOrder()->first()->id;

    return [
        'registration_zip_code'	=> $zip,
        // 'owner_id'				=> $citizen,
    ];
});
