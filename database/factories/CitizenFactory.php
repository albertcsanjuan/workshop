<?php

use Faker\Generator as Faker;

$factory->define(App\Citizen::class, function (Faker $faker) {
	$zip = App\City::inRandomOrder()->first()->zip;
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'address' => $faker->streetAddress,
        'gender' => $faker->randomElement(array('M','F')),
        'birthdate' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'zip' => $zip,
    ];
});
