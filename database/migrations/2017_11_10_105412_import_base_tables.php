<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $lines = preg_split('/\r\n|\n|\r/', trim(file_get_contents(base_path('database/scripts/states.sql'))));
        // foreach ($lines as $line) {
        //     DB::statement($line);
        // }
        // $lines = preg_split('/\r\n|\n|\r/', trim(file_get_contents(base_path('database/scripts/cities.sql'))));
        // foreach ($lines as $line) {
        //     DB::statement($line);
        // }
        // $lines = preg_split('/\r\n|\n|\r/', trim(file_get_contents(base_path('database/scripts/vehicle_models.sql'))));
        // foreach ($lines as $line) {
        //     DB::statement($line);
        // }
        DB::unprepared(file_get_contents(base_path('database/scripts/states.sql')));
        DB::unprepared(file_get_contents(base_path('database/scripts/cities.sql')));
        DB::unprepared(file_get_contents(base_path('database/scripts/vehicle_models.sql')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
