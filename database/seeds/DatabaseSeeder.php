<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$count = 40;



    	$this->call(TrafficRuleSeeder::class);



    	$this->command->info("Seeding: CitizenFactory");
    	$bar = $this->command->getOutput()->createProgressBar($count);

    	for ($i=0; $i < $count ; $i++) {
	    	$c = factory(App\Citizen::class)->create();
			if ($c->age > 18) {
				
				$l = factory(\App\License::class)->make();
				$l = $c->license()->updateOrCreate(
					[ 'license_no' => $l->license_no ],
					$l->toArray()
				);

				$l->registrations()->delete();

				$l->registrations()->save(factory(\App\LicenseRegistration::class)->make([
					'registration_date' => $l->created_at,
					'expiry_date' => $l->created_at->addYears(3)
				]));	

			}
			$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info("\n");


    	$this->command->info("Seeding: VehicleFactory");
    	$bar = $this->command->getOutput()->createProgressBar($count);

    	for ($i=0; $i < $count ; $i++) {
        	$v = factory(App\Vehicle::class)->make();
        	$v = \App\Vehicle::updateOrCreate(
        		[ 'plate_number' => $v->plate_number ],
        		$v->toArray()
        	);

	        $owner = App\Citizen::inRandomOrder()->first()->id;
	        $inityear = $v->year;
	        $inityear_beginning = Carbon\Carbon::parse('first day of ' . $inityear);
	        $random_days = rand(0,364);
	        $reg_date = $inityear_beginning->addDays($random_days);

	        for ($j = $inityear; $j < 2017; $j++) {
	        	$rd = Carbon\Carbon::createFromDate($j,$reg_date->month, $reg_date->day);
	        	$ed = Carbon\Carbon::createFromDate($j+1,$reg_date->month, $reg_date->day);
	        	if (rand(0,10) < 3) {
	        		$owner = $c = App\Citizen::inRandomOrder()->first()->id;
	        	}

	        	$v->registrations()->delete();
	        	$v->violations()->delete();

	        	$v->registrations()->save(factory(App\VehicleRegistration::class)->make([
	        			'owner_id' => $owner,
	        			'registration_date'	=> $rd,
	        			'expiry_date'	=> $ed
	        	]));
	        }

	        if (rand(0,1) == 1) {
	        	$offenses = rand(1,10);
	        	$l = App\License::inRandomOrder()->first()->id;

	        	for ($k=0; $k<$offenses; $k++) {
	        		$v->violations()->save(factory(App\Violation::class)->make([
	        			'license_id' => $l
	        		]));
	        	}	
	        }

	        $bar->advance();
	    }
	    $bar->finish();
	    $this->command->info("\n");



    }
}
