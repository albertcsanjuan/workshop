@extends('layout')
@section('body')
	<section class="section">
		<div class="container">
			<h1 class="title">Edit <small class="has-text-grey-lighter">citizen#{{ $citizen->id }}</small></h1>
			<h2 class="subtitle">/resources/views/citizen/edit.blade.php</h2>
			<hr>
			<div class="buttons">
				<a href="{{ route('citizen.index') }}" class="button">Back to index</a>
			</div>
		</div>
		<div class="container">
			<div class="columns">
				<div class="column is-one-third">
					<div class="box">
						<form method="post" action="{{ route('citizen.update', ['citizen' => $citizen->id]) }}" novalidate>
							{{ csrf_field() }}

							<div class="field">
								<label class="label">First Name</label>
								<input class="input" type="text" name="first_name" required minlength="1" maxlength="50" value="{{ $citizen->first_name }}">
							</div>

							<div class="field">
								<label class="label">Middle Name</label>
								<input class="input" type="text" name="middle_name" minlength="1" maxlength="50" value="{{ $citizen->middle_name }}">
							</div>

							<div class="field">
								<label class="label">Last Name</label>
								<input class="input" type="text" name="last_name" required minlength="1" maxlength="50" value="{{ $citizen->last_name }}">
							</div>

							<div class="field">
								<label class="label">Gender</label>
								<p class="select">
									<select name="gender" required>
										<option value="">Select gender</option>
										<option value="M" @if($citizen->gender=='M') selected @endif>Male</option>
										<option value="F" {{ ($citizen->gender=='F')? 'selected' : '' }}>Female</option>
									</select>
								</p>
							</div>

							<div class="field">
								<label class="label">Birthdate</label>
								<input class="input" type="date" name="birthdate" required value="{{ $citizen->birthdate }}">
							</div>

							<div class="field">
								<label class="label">Street Address</label>
								<textarea class="input" name="address" required maxlength="256">{{ $citizen->address }}</textarea>
							</div>

							<div class="field">
								<label class="label" id="zip_label">ZIP Code</label>
								<input class="input" type="text" name="zip" required minlength="1" maxlength="5" value="{{ $citizen->zip }}">
							</div>

							<input type="hidden" name="submitted_by" value="Nikko Antonio">
							
							<div class="buttons">
								<button class="button is-primary" type="submit">Save</button>
								<a href="{{ route('citizen.index') }}" class="button">Cancel</a>
							</div>
							
						</form>
					</div>
				</div>
				<div class="column">
					@if ($errors->any())
					<div class="content">
						<div class="notification is-danger">
							<h3 class="title">The form has validation errors.</h3>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</section>
@endsection