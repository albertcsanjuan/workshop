@extends('layout')
@section('body')
	<section class="section">
		<div class="container">
			<h1 class="title">Citizens Index</small></h1>
			<h2 class="subtitle">/resources/views/citizen/index.blade.php</h2>
			<hr>
			<div class="buttons">
				<a href="{{ route('citizen.create') }}" class="button is-primary">Create</a>
			</div>
		</div>
		<div class="container">
			<div class="columns">
				<div class="column">
					<div class="">
						@if($citizens->count())
						<table class="table is-hoverable is-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th class="has-text-centered">Actions</th>
									<th>First Name</hd>
									<th>Middle Name</th>
									<th>Last Name</th>
									<th>Birth Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($citizens as $citizen)
									<tr>
										<td>{{ $citizen->id }}</td>
										<td>
											<div>
											<a href="{{ route('citizen.show', ['citizen' => $citizen->id] ) }}">Show</a>
											<a href="{{ route('citizen.edit', ['citizen' => $citizen->id] ) }}">Edit</a>
											<a href="{{ route('citizen.destroy', ['citizen' => $citizen->id] ) }}">Delete</a>
											</div>
										</td>
										<td>{{ $citizen->first_name }}</td>
										<td>{{ $citizen->middle_name }}</td>
										<td>{{ $citizen->last_name }}</td>
										<td>{{ $citizen->birthdate }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<p>No citizens yet. Click the button above to create a new one.</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection