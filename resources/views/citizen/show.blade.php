@extends('layout')
@section('body')
	<section class="section">
		<div class="container">
			<h1 class="title">Show <small class="has-text-grey-lighter">citizen#{{ $citizen->id }}</small></h1>
			<h2 class="subtitle">/resources/views/citizen/show.blade.php</h2>
			<hr>
			<div class="buttons">
				<a href="{{ route('citizen.index') }}" class="button">Back to index</a>
			</div>
		</div>
		<div class="container">
			<div class="columns">
				<div class="column is-one-third">
					<div class="">
						<table class="table is-hoverable is-bordered">
							<tr>
								<th>ID</th>
								<td>{{ $citizen->id }}</td>
							</tr>
							<tr>
								<th>First Name</th>
								<td>{{ $citizen->first_name }}</td>
							</tr>
							<tr>
								<th>Middle Name</th>
								<td>{{ $citizen->middle_name }}</td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td>{{ $citizen->last_name }}</td>
							</tr>
							<tr>
								<th>Gender</th>
								<td>{{ $citizen->gender }}</td>
							</tr>
							<tr>
								<th>Birthdate</th>
								<td>{{ $citizen->birthdate }}</td>
							</tr>
							<tr>
								<th>Street Address</th>
								<td>{{ $citizen->address }}</td>
							</tr>
							<tr>
								<th>ZIP Code</th>
								<td>{{ $citizen->zip }}</td>
							</tr>
							<tr>
								<th>Created At</th>
								<td>{{ $citizen->created_at }}</td>
							</tr>
							<tr>
								<th>Updated At</th>
								<td>{{ $citizen->updated_at }}</td>
							</tr>
						</table>
					</div>
					<div class="buttons">
						<a href="{{ route('citizen.edit', ['citizen' => $citizen->id]) }}" class="button is-primary">Edit</a>
						<a href="{{ route('citizen.destroy', ['citizen' => $citizen->id]) }}" class="button is-danger">Delete</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection