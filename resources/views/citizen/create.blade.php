@extends('layout')
@section('body')
<section class="section">
	<div class="container">
		<h1 class="title">Citizen Entry</h1>
		<h2 class="subtitle">/resources/views/citizen/create.blade.php</h2>
		<hr>
		<div class="buttons">
			<a href="{{ route('citizen.index') }}" class="button">Back to index</a>
		</div>
	</div>
	<form method="post" action="{{ route('citizen.store') }}" novalidate>
		<div class="container">
			<div class="columns">
				<div class="column is-one-third">
					<div class="box">
						
						{{ csrf_field() }}

						<h2 class="subtitle">Citizen Information</h2>

						<div class="field">
							<label class="label">First Name</label>
							<input class="input" type="text" name="first_name" required minlength="1" maxlength="50">
						</div>

						<div class="field">
							<label class="label">Middle Name</label>
							<input class="input" type="text" name="middle_name" minlength="1" maxlength="50">
						</div>

						<div class="field">
							<label class="label">Last Name</label>
							<input class="input" type="text" name="last_name" required minlength="1" maxlength="50">
						</div>

						<div class="field">
							<label class="label">Gender</label>
							<p class="select">
								<select name="gender" required>
									<option value="">Select gender</option>
									<option value="M">Male</option>
									<option value="F">Female</option>
								</select>
							</p>
						</div>

						<div class="field">
							<label class="label">Birthdate</label>
							<input class="input" type="date" name="birthdate" required>
						</div>

						<div class="field">
							<label class="label">Street Address</label>
							<textarea class="input" name="address" required maxlength="256"></textarea>
						</div>

						<div class="field">
							<label class="label" id="zip_label">ZIP Code</label>
							<input class="input" type="text" name="zip" required minlength="1" maxlength="5">
						</div>
						<hr>
						<label class="label">More Options</label>
						
						<label class="checkbox">
						  <input type="checkbox" id="add-license">
						  Register License
						</label>
						<label class="checkbox">
						  <input type="checkbox" id="add-vehicle">
						  Register New Vehicle
						</label>
						
					</div>
				</div>
				<div class="column is-one-third" style="display:none;" id="vehicleForm">
					<div class="box">
						
						<h2 class="subtitle">Register New Vehicle for this Citizen</h2>

						<div class="field">
							<label class="label">Model Year</label>
							<input class="input" type="number" name="year" required value="" max="2018" min="1900" id="model-year">
						</div>

						<div class="field">
							<label class="label">Make &amp; Model</label>
							<p class="select">
								<select name="model" required id="model_dropdown">
									<option value="">Select year model</option>
								</select>
							</p>
						</div>

						<div class="field">
							<label class="label">Displacement</label>
							<input class="input" type="number" name="displacement" required value="2.0" max="10" min="0.1" step="0.1">
						</div>

						<div class="field">
							<label class="label">Plate Number</label>
							<input class="input" type="text" name="plate_number" minlength="1" maxlength="50">
						</div>

						<div class="field">
							<label class="label">Color</label>
							<input class="input" type="text" name="color" minlength="1" maxlength="50">
						</div>




					</div>
				</div>
				<div class="column">
					@if ($errors->any())
					<div class="content">
						<div class="notification is-danger">
							<h3 class="title">The form has validation errors.</h3>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
				</div>
			</div>
		
			<input type="hidden" name="submitted_by" value="Workshop Participant">

			<div class="box has-text-right" style="background: #eee;">
				<button class="button is-primary" type="submit">Create</button>
				<a href="{{ route('citizen.index') }}" class="button">Cancel</a>
			</div>

		</div>
	</form>
</section>
@endsection