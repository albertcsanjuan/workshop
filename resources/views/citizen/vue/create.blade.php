@extends('layout', ['active' => 'citizens-vue'])
@section('body')
	<section class="section">
		<div class="container">
			<h1 class="title">Create and Index (using Vue.js)</h1>
			<h2 class="subtitle">/resources/views/vue-js/create.blade.php</h2>
			<hr>
		</div>
		<div class="container">
			<div class="columns">
				<div class="column is-one-third">
					<div class="box">
						<form novalidate id="create">
							<div class="field">
								<label class="label">First Name</label>
								<input class="input" type="text" name="first_name" required minlength="1" maxlength="50">
							</div>

							<div class="field">
								<label class="label">Middle Name</label>
								<input class="input" type="text" name="middle_name" minlength="1" maxlength="50">
							</div>

							<div class="field">
								<label class="label">Last Name</label>
								<input class="input" type="text" name="last_name" required minlength="1" maxlength="50">
							</div>

							<div class="field">
								<label class="label">Gender</label>
								<p class="select">
									<select name="gender" required>
										<option value="">Select gender</option>
										<option value="M">Male</option>
										<option value="F">Female</option>
									</select>
								</p>
							</div>

							<div class="field">
								<label class="label">Birthdate</label>
								<input class="input" type="date" name="birthdate" required>
							</div>

							<div class="field">
								<label class="label">Street Address</label>
								<textarea class="input" name="address" required maxlength="256"></textarea>
							</div>

							<div class="field">
								<label class="label" id="zip_label">ZIP Code</label>
								<input class="input" type="text" name="zip" required minlength="1" maxlength="5">
							</div>

							<input type="hidden" name="submitted_by" value="Nikko Antonio">
							
							<div class="buttons">
								<button class="button is-primary" type="submit" @click.prevent="submit()" @keyup.enter.prevent="submit()" @keyup.space.prevent="submit()">Create</button>
								<button class="button" @click.prevent="resetForm()" @keyup.enter.prevent="resetForm()" @keyup.space.prevent="resetForm()">Reset</button>
							</div>
							
						</form>
					</div>
				</div>
				<div class="column is-half">
					<div class="content" id="success" v-if="visible" v-cloak>
						<div class="notification is-success">
							<p>A new citizen was created successfully.</p>
						</div>
					</div>
					<div class="content" id="errors" v-if="visible" v-cloak>
						<div class="notification is-danger">
							<p>The form has validation errors.</p>
							<ul>
								<li v-text="errors.first_name">First name</li>
								<li v-text="errors.middle_name">Middle name</li>
								<li v-text="errors.last_name">Last name</li>
								<li v-text="errors.gender">Gender</li>
								<li v-text="errors.birthdate">Birthdate</li>
								<li v-text="errors.address">Street Address</li>
								<li v-text="errors.zip">ZIP Code</li>
							</ul>
						</div>
					</div>
					<div class="is-paddingless is-marginless" id="list">
						<div class="notification is-warning" v-show="false">
							<p>Please wait for scripts to load...</p>
						</div>
						<table class="table is-hoverable is-bordered is-fullwidth" v-cloak>
							<thead>
								<tr>
									<th>ID</th>
									<th>First Name</hd>
									<th>Middle Name</th>
									<th>Last Name</th>
									<th>Birth Date</th>
								</tr>
							</thead>
							<tbody>
									<tr v-for="citizen in citizens">
										<td v-text="citizen.id">ID</td>
										<td v-text="citizen.first_name">First Name</td>
										<td v-text="citizen.middle_name">Middle Name</td>
										<td v-text="citizen.last_name">Last Name</td>
										<td v-text="citizen.birthdate">Birth Date</td>
									</tr>
							</tbody>
						</table>
						<div class="notification" v-cloak>
							<p>This table is updated using JavaScript to show the last 10 records.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="https://unpkg.com/vue"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
	<script type="text/javascript">

		var list = new Vue({
			el: '#list',
			data: {
				citizens: {},
				url: '/vue/citizen',
				status: 'unfetched'
			},
			methods: {
				pull : function (url = this.url) {
			    	this.status = 'fetching';
			        let promise = axios.get(url)
			            .then(function (response) {
			                this.citizens = response.data;
			                this.status = 'success';
			                }.bind(this))
			            .catch(function (error) {
			                this.status = 'fail';
			                }.bind(this))
			            ;
			        return promise;
				}
			},
			mounted: function () {
				this.pull();
			}
		});

		var formErrors = new Vue({
			el: '#errors',
			data : {
				errors: {},
				visible: false
			},
			methods: {
				clear: function () {
					this.errors = {};
					this.hide();
				},
				show: function (errors) {
					this.errors = errors;
					this.visible = true;
				},
				hide: function () {
					this.visible = false;
				}
			}
		});

		var formSuccess = new Vue({
			el: '#success',
			data : {
				visible: false
			},
			methods: {
				show: function () {
					this.visible = true;
					setTimeout(function () {this.hide();}.bind(this), 5000);
				},
				hide: function () {
					this.visible = false;
				}
			}
		});

		var form = new Vue({
			el: '#create',
			data: {
				form_id: 'create',
				errors: undefined,
				status: 'unsubmitted',
				url: '/vue/citizen',
			},
			methods: {
				getFormInputs : function () {
					data = new FormData(document.getElementById(this.form_id));
					console.log(data);
					return data;
				},
				resetForm : function () {
					console.log('Clearing the form...');
					document.getElementById(this.form_id).reset();
					this.clearErrors();
				},
				clearErrors : function () {
					console.log('Clearing the errors...');
					formErrors.clear();
				},
				displayErrors : function () {
					formErrors.show(this.errors);
				},
				displaySuccess : function () {
					formSuccess.show();
				},
				refreshList : function () {
					list.pull();
				},
				submit : function (url = this.url, data = this.getFormInputs()) {
					this.clearErrors();
					this.status = 'submitting';
					let promise = axios.post(url, data)
					    .then(function (response) {
					    	console.log(response);
					        this.citizens = response.data;
					        this.status = 'success';
					        this.displaySuccess();
					        this.refreshList();
					        this.resetForm();
					        }.bind(this))
					    .catch(function (error) {
					    	if (error.response.status >= 400 && error.response.status < 500) {
					    	// 4xx Client errors
					    	    this.status = 'error';
					    	    if(error.response.status == 422) {
					    	    // 422 - Unprocessable Entity
					    	    // request has failed validation rules
					    	        this.status = 'invalid';
					    	        this.errors = error.response.data.errors;
					    	        this.clearErrors();
					    	        this.displayErrors();
					    	        this.refreshList();
					    	    }
					    	}
					        this.status = 'fail';
					        }.bind(this))
					    ;
					return promise;
				},
			}
		});
	</script>
@endsection