<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css">
		<style type="text/css">
			[v-cloak] {display: none;}
		</style>
	</head>
	<body>
		<section class="hero is-info">
			<div class="hero-body">
				<div class="container">
					<h1 class="title">Citizen Registry</h1>
					<h2 class="subtitle">RCDTA8594 - Third Regional Technical Training</h2>
				</div>
			</div>
			<div class="hero-foot">
			    <nav class="tabs is-boxed">
			      <div class="container">
			        <ul>
			          <li class="{{ $active == "" ? 'is-active' : '' }}"><a href="/">Home</a></li>
			          <li class="{{ $active == 'citizens' ? 'is-active' : '' }}"><a href="{{ route('citizen.index') }}">Citizens</a></li>
			          <li class="{{ $active == 'citizens-vue' ? 'is-active' : '' }}"><a href="{{ route('citizen.create.vue') }}">Citizens (Vue)</a></li>
			          <li class="{{ $active == 'licenses' ? 'is-active' : '' }}"><a>Licenses</a></li>
			          <li class="{{ $active == 'vehicles' ? 'is-active' : '' }}"><a>Vehicles</a></li>
			        </ul>
			      </div>
			    </nav>
			  </div>
		</section>

		@yield('body')
		
		{{-- Have external script files? put them below here --}}
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
			$(document).ready(function() {
				$("#add-vehicle").on('change',function () {

					if ($(this).is(":checked")) {
						$("#vehicleForm").slideDown();	
					} else {
						$("#vehicleForm").slideUp();
					}
					
				});
				$("#model-year").change(function () {
					var selected_year = $(this).val();

					$.ajax({
						url: '/listmodels/year/' + selected_year,
						method: 'get',
						success: function (data) {
							$("#model_dropdown").html(data);
						}
					});

				});


			});
		</script>
	</body>
</html>