@extends('layout')
@section('body')
		<section class="section">
			<div class="container">
				<h1 class="title">Welcome</h1>
				<h2 class="subtitle">/resources/views/citizen/welcome.blade.php</h2>
				<hr>
				<ul>
					<li><a href="{{ route('citizen.index') }}">Introduction to Laravel (example) >></a></li>
					<li><a href="{{ route('citizen.create.vue') }}">Introduction to Vue.js (example) >></a></li>
				</ul>
			</div>
		</section>
@endsection