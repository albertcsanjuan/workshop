<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Citizen - Response</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
    <style>
      .middle-card {
        max-width: 500px;
        margin: 10px auto;
      }
      header {
        text-align: center;
      }
      body {
        background: #eee;
      }
    </style>
  </head>
  <body>
  <section class="section">
    <div class="box middle-card">
      <header>
      <h1 class="title">
        New citizen
      </h1>
      <p class="subtitle">
        You have submitted the following information:
      </p>
      </header>
      <hr>
    <table class="table is-fullwidth">
      <thead>
        <tr>
          <th>Field</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>First Name</th>
          <td>{{ $r->first_name or "Not provided" }}</td>
        </tr>
        <tr>
          <th>Middle Name</th>
          <td>{{ $r->middle_name or "Not provided (optional)" }}</td>
        </tr>
        <tr>
          <th>Last Name</th>
          <td>{{ $r->last_name or "Not provided" }}</td>
        </tr>
        <tr>
          <th>Gender</th>
          <td>{{ $r->gender or "Not provided" }}</td>
        </tr>
        <tr>
          <th>Birthdate</th>
          <td>{{ $r->birthdate or "Not provided" }}</td>
        </tr>
        <tr>
          <th>Street Address</th>
          <td>{{ $r->address or "Not provided" }}</td>
        </tr>
        <tr>
          <th>ZIP Code</th>
          <td>{{ $r->zip or "Not provided" }}</td>
        </tr>
        <tr>
          <th>Submitted by</th>
          <td>{{ $r->submitted_by or "Not provided" }}</td>
        </tr>
      </tbody>
    </table>
    @if ($r->filled(['first_name','middle_name','last_name','gender','birthdate','address','zip','submitted_by']))
    <div class="notification is-success">Your data is valid!</div>
    @else
    <div class="notification is-warning">Your data is invalid. Please revisit your form and try again.</div>
    @endif
  </div>
  </section>
  </body>
</html>