<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome',['active' => '']);
});

Route::post('/test/citizens',"TestController@testSubmit");

// Citizen
Route::get('/citizen', 'CitizenController@index')->name('citizen.index');
Route::post('/citizen', 'CitizenController@store')->name('citizen.store');
Route::get('/citizen/create', 'CitizenController@create')->name('citizen.create');
Route::get('/citizen/{citizen}', 'CitizenController@show')->name('citizen.show');
Route::get('/citizen/{citizen}/edit', 'CitizenController@edit')->name('citizen.edit');
Route::get('/citizen/{citizen}/delete', 'CitizenController@destroy')->name('citizen.destroy');
Route::post('/citizen/{citizen}', 'CitizenController@update')->name('citizen.update');

// Using Vue.js
Route::get('/vue/citizen', 'CitizenVueController@index')->name('citizen.index.vue');
Route::post('/vue/citizen', 'CitizenVueController@store')->name('citizen.store.vue');
Route::get('/vue/citizen/create', 'CitizenVueController@create')->name('citizen.create.vue');

Route::get('/listmodels/year/{year}',function ($year) {
	$models = App\VehicleModel::where('year',$year)->get();
	$options = "";
	foreach ($models as $m) {
		$options .= "<option value='" . $m->id . "'>{$m->make} {$m->model} {$m->year}</option>";
	}
	return $options;
});
